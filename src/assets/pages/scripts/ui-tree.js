var UITree = function () {

    var handleSample1 = function () {

        $('#tree_1').jstree({
            "core": {
                "themes": {
                    "responsive": false
                }
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            "plugins": ["types"]
        });

        // handle link clicks in tree nodes(support target="_blank" as well)
        $('#tree_1').on('select_node.jstree', function (e, data) {
            var link = $('#' + data.selected).find('a');
            if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
                if (link.attr("target") == "_blank") {
                    link.attr("href").target = "_blank";
                }
                document.location.href = link.attr("href");
                return false;
            }
        });
    }

    var handleSample2 = function () {
        $('#treeMap').jstree({
            'plugins': ["wholerow", "types"],
            'core': {
                "themes": {
                    "responsive": false
                },
                'data': [{
                        "text": "Raw Enquiry",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "RW002",
                                "icon": "fa fa-file icon-state-default",
                                "state": {
                                    "opened": true
                                }
                            }, {
                                "text": "RW003",
                                "icon": "fa fa-file icon-state-default"
                            }, {
                                "text": "RW004",
                                "icon": "fa fa-file icon-state-default",
                            }]
                    },
                    {
                        "text": "RFQ",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "1st Step Tech LLc",
                                "icon": "fa fa-file icon-state-default",
                                "state": {
                                    "selected": false
                                }
                            }, {
                                "text": "3M UK Plc",
                                "icon": "fa fa-file icon-state-default"
                            }]
                    },
                    {
                        "text": "Quotes",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "QTE00014",
                                "icon": "fa fa-file icon-state-default",
                                "state": {
                                    "selected": false
                                }
                            }, {
                                "text": "QTE00015",
                                "icon": "fa fa-file icon-state-default"
                            }, {
                                "text": "QTE00016",
                                "icon": "fa fa-file icon-state-default",
                            }]
                    },
                ]
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    }
        var handleSample2 = function () {
        $('#sourcingMap').jstree({
            'plugins': ["wholerow", "types"],
            'core': {
                "themes": {
                    "responsive": false
                },
                'data': [{
                        "text": "ITO's",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "1ST Step Tech LLC ",
                                "icon": "fa fa-file icon-state-default",
                                "state": {
                                    "opened": true
                                }
                            }, {
                                "text": "2M Power Systems Ltd ",
                                "icon": "fa fa-file icon-state-default"
                            },
                            {
                                "text": "3FFF -FIRE FIGHTING FOAMS ",
                                "icon": "fa fa-file icon-state-default"
                            },
                            {
                                "text": "3M UK PIC ",
                                "icon": "fa fa-file icon-state-default"
                            }, {
                                "text": "4OURHOUSE.CO.UK",
                                "icon": "fa fa-file icon-state-default",
                            }]
                    },
                    {
                        "text": "Quotes",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "QTE00014",
                                "icon": "fa fa-file icon-state-default",
                                "state": {
                                    "selected": false
                                }
                            }, {
                                "text": "QTE00015",
                                "icon": "fa fa-file icon-state-default"
                            }, {
                                "text": "QTE00016",
                                "icon": "fa fa-file icon-state-default",
                            }]
                    },
                ]
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    }
    var handleSample9 = function () {
        $('#treeSecurity').jstree({
            'plugins': ["wholerow","checkbox", "types"],
            'core': {
                "themes": {
                    "responsive": false
                },
                'data': [{
                        "text": "Teams",
                         "icon": "fa fa-users icon-state-default",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "Blue Team",
                                "state": {
                                    "opened": true
                                }
                            }, {
                                "text": "Red Team",
                            }, {
                                "text": "Green Team",
                            }]
                    },
                    {
                        "text": "Users",
                         "icon": "fa fa-user icon-state-default",
                        "state": {
                            "opened": true
                        },
                        "children": [{
                                "text": "Andi Fraser",
                                "state": {
                                    "selected": false
                                }
                            }, {
                                "text": "Neil Haskins",
                            }]
                    },
                ]
            },
            "types": {
                "default": {
                    "icon": "fa fa-use icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-use icon-state-warning icon-lg"
                }
            }
        });
    }

    var contextualMenuSample = function () {

        $("#tree_3").jstree({
            "core": {
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': [{
                        "text": "Parent Node",
                        "children": [{
                                "text": "Initially selected",
                                "state": {
                                    "selected": true
                                }
                            }, {
                                "text": "Custom Icon",
                                "icon": "fa fa-warning icon-state-danger"
                            }, {
                                "text": "Initially open",
                                "icon": "fa fa-folder icon-state-success",
                                "state": {
                                    "opened": true
                                },
                                "children": [
                                    {"text": "Another node", "icon": "fa fa-file icon-state-warning"}
                                ]
                            }, {
                                "text": "Another Custom Icon",
                                "icon": "fa fa-warning icon-state-warning"
                            }, {
                                "text": "Disabled Node",
                                "icon": "fa fa-check icon-state-success",
                                "state": {
                                    "disabled": true
                                }
                            }, {
                                "text": "Sub Nodes",
                                "icon": "fa fa-folder icon-state-danger",
                                "children": [
                                    {"text": "Item 1", "icon": "fa fa-file icon-state-warning"},
                                    {"text": "Item 2", "icon": "fa fa-file icon-state-success"},
                                    {"text": "Item 3", "icon": "fa fa-file icon-state-default"},
                                    {"text": "Item 4", "icon": "fa fa-file icon-state-danger"},
                                    {"text": "Item 5", "icon": "fa fa-file icon-state-info"}
                                ]
                            }]
                    },
                    "Another Node"
                ]
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state": {"key": "demo2"},
            "plugins": ["contextmenu", "dnd", "state", "types"]
        });

    }

    var ajaxTreeSample = function () {

        $("#tree_4").jstree({
            "core": {
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': {
                    'url': function (node) {
                        return '../demo/jstree_ajax_data.php';
                    },
                    'data': function (node) {
                        return {'parent': node.id};
                    }
                }
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state": {"key": "demo3"},
            "plugins": ["dnd", "state", "types"]
        });

    }


    return {
        //main function to initiate the module
        init: function () {

            handleSample1();
            handleSample2();
            handleSample9();
            contextualMenuSample();
            ajaxTreeSample();

        }

    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function () {
        UITree.init();
    });
}