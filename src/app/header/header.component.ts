import { Component, OnInit,ViewChild,ViewEncapsulation,Renderer,ElementRef } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  @ViewChild('sidebar-toggler') el: ElementRef;

  constructor(private renderer:Renderer,
              private elementRef:ElementRef) { }

  ngOnInit() {
  }
  toggleSibebar(event){
    console.log('submit')
    // event.target.classList.add('class3'); // To ADD
    let toggle = document.getElementsByClassName('sidebar-wrapper')[0];
    let toolbar = document.getElementsByClassName('content-wrapper')[0];

     if(toggle.classList.contains('closed')) {
      toggle.classList.remove('closed');
      toolbar.classList.remove('expanded');
    } else {
      toggle.classList.add('closed');
      toolbar.classList.add('expanded');

    }


  }

}
