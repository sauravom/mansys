import { Component, OnInit,ComponentFactoryResolver,ViewContainerRef,ViewChild } from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  //  @ViewChild('subContainer1', {read: ViewContainerRef}) subContainer1: ViewContainerRef;

  constructor(private compFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    // let compFactory;
    //  compFactory = this.compFactoryResolver.resolveComponentFactory(SidebarComponent);
    //   this.subContainer1.createComponent(compFactory);
  }
  

}
