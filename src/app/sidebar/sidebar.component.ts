import { Component, OnInit, ViewChild,ViewContainerRef, ElementRef,ViewEncapsulation,ComponentFactoryResolver,Renderer } from '@angular/core';
import { GetJSONService } from '../services/get-json.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  @ViewChild('sidebar', { read: ViewContainerRef }) sidebar: ElementRef;

  constructor(public getJSONService: GetJSONService,
              private componentFactoryResolver:ComponentFactoryResolver,
              private elementRef:ElementRef,
              private renderer: Renderer,
              private viewContainerRef:ViewContainerRef) { 
              }
  sideBarData: string = '';
  public data1
  ngOnInit() {
    this.sideBarData += '<li class="sidebar-toggler-wrapper hide" >' +
      '<div class="sidebar-toggler" > </div>' +
      '</li>' +
      '<li class="sidebar-search-wrapper"></li>';
    this.sideBarData += '<li class="nav-item start">' +
      '<a class="nav-link home name" (click)="HomeButton(event)">' +
      '<i class="fa fa-home"></i>' +
      '<span class="title">Home</span>' +
      '</a>' +
      '</li>';
    this.getJSONService.getData()
      .subscribe(response => {
        var value=response.datas
        if (value.length > 0) {
          for (var i = 0; i < value.length; i++) {
            if (value[i].state)
            {
                this.sideBarData += '<li class="nav-item" >' +
                        '<a (click)="parentNav()" routerLink="/' + value[i].state + '" class="nav-link nav-toggle ' + value[i].key + '" >' +
                        '<i class="fa fa-' + value[i].label_Icon + '"></i>' +
                        '<span class="title">' + value[i].label + '</span>';
                if (this.checkDataExist(value[i]))
                {
                   var data1 = this.data(value[i]);
                }        
                this.sideBarData += '</a></li>';
            } else
            {
                this.sideBarData += '<li class="nav-item" >' +
                        '<a  class="nav-link nav-toggle ' + value[i].key + '" (click)="parentNav($event)">' +
                        '<i class="fa fa-' + value[i].label_Icon + '"></i>' +
                        '<span class="title">' + value[i].label + '</span>';
                if (this.checkDataExist(value[i]))
                {
                   var data1 = this.data(value[i].data);
                } 
                this.sideBarData += '</a></li>';
            }
          }
        }
      })
    // this.sidebar.nativeElement.append(this.sideBarData.concat(this.data1) )
  }
  data(arraySet){
    var v=[];
        v =arraySet
        this.sideBarData += '<ul class="sub-menu">';
        if(v.length>0){
          for(var i=0;i<v.length;i++){
            if (v[i].state) {
                this.sideBarData +=
                        '<li class="nav-item">' +
                        '<a (click)="childNav(event)" [routerLink]="[/' + v[i].state + ']" class="nav-link nav-toggle ' + v[i].key + '">' +
                        '<i class="fa fa-' + v[i].label_Icon + '"></i>' +
                        '<span class="title">' + v[i].label + '</span>';
                if (this.checkDataExist(v)) {
                    this.data1 = this.data2(v);
                }
                this.sideBarData += '</a></li>';
            } else {
                this.sideBarData +=
                        '<li class="nav-item">' +
                        '<a (click)="childNav(event)" class="nav-link nav-toggle ' + v[i].key + '">' +
                        '<i class="fa fa-' + v[i].label_Icon + '"></i>' +
                        '<span class="title">' + v[i].label + '</span>';
                if (this.checkDataExist(v[i])) {
                    this.data1 = this.data2(v[i].data);
                }
                this.sideBarData += '</a></li>';
            }
          }
        }
        this.sideBarData += '</ul>';
        return this.sideBarData;
    };
    data2 = function (arraySet) {
      var v1=arraySet
        this.sideBarData += '<ul class="sub-menu">';
        if(v1.length>0){
          for(var i=0;i<v1.length;i++){
            if (v1[i].state) {
                this.sideBarData +=
                        '<li class="nav-item">' +
                        '<a (click)="childNav1(event)" [routerLink]="[/' + v1[i].state + ']" class="nav-link nav-toggle ' + v1[i].key + '">' +
                        '<i class="fa fa-' + v1[i].label_Icon + '"></i>' +
                        '<span class="title">' + v1[i].label + '</span>' +
                        '<span class=""></span>' +
                        '</a>';
                if (this.checkDataExist(v1)) {
                  console.log('checkDataExist')
                    this.data1 = this.data2(v1);
                }
                this.sideBarData += '</li>';
            } else {
                this.sideBarData +=
                        '<li class="nav-item">' +
                        '<a (click)="childNav1(event)" class="nav-link nav-toggle ' + v1[i].key + '">' +
                        '<i class="fa fa-' + v1[i].label_Icon + '"></i>' +
                        '<span class="title">' + v1[i].label + '</span>' +
                        '<span class=""></span>' +
                        '</a>';
                if (this.checkDataExist(v1)) {
                    this.data1 = this.data2(v1);
                }
                this.sideBarData += '</li>';
            }
        }
        }
        this.sideBarData += '</ul>';
        return this.sideBarData;
    }
  checkDataExist(arraySet) {
        if (arraySet.hasOwnProperty('data')) {
            this.sideBarData += '<span class="arrow"></span><span class=""></span>';
            return true;
        } else {
            this.sideBarData += '<span class=""></span>';
            return false;
        }
    }
    parentNav(event){
      alert(event)
    }
}
