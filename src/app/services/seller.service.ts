import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SellerService {

  constructor(public http: Http) { }
  sellerData(){
    return this.http.get('data/seller.json')
    .map(
        result =>
        {
          return result.json()
        });
  }

}
