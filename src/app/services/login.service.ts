import { Injectable } from '@angular/core';
import {AppConstant} from '.././app.constant';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  constructor(public http: Http) { }
  loginData(data){
    return this.http.post(
      AppConstant.API_DATA + 'validate',
      data)
    .map(
        result =>
        {
          return result.json()
        });
  }
  signInData(userData,headerData){
    return this.http.post(
      AppConstant.API_DATA+'authenticate',
      userData,{headers: headerData}
    )
    .map(
        result =>
        {
          return result.json()
        });
  }

}
