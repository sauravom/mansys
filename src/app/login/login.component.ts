import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { FormsModule, Validators } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public loginService: LoginService,
              private toasterService: ToasterService,
              private router: Router,
              private localStorageService: LocalStorageService) {
               this.toasterService = toasterService;
  }

  public companyData;
  public userData;
  public headerData={}
  public selectCompany;
  ngOnInit() {
  }
  displayCompany: boolean = false;
  submit(formCtrl) {
    console.log(formCtrl.value)
    this.userData=formCtrl.value
    this.loginService.loginData(this.userData)
      .subscribe(response => {
        console.log(response)
        this.displayCompany = true
        this.companyData = response.data.companies;
        this.toasterService.pop('success', response.message);
        this.headerData = {
                            "X-API-Key": response.data.apiKey,
                            "X-Profile-Key": "",
                            "X-Company-Key": response.data.groupKey,
                            "Content-Type": "application/json"
                        };
        this.selectCompany = this.companyData[0].companyKey
      }, err => {
        console.log(err)
        this.toasterService.pop('error', 'Invalid Credential');
      },
      () => console.log('Get json Complete')
      );
  }
  goToLogin(){
    this.displayCompany=false;
    console.log(localStorage.getItem(this.mansysCloudAccessToken))
  }
  public mansysCloudTokenType
  public mansysCloudAccessToken
  public mansysCloudProfileName
  public mansysCloudFullName
  signIn(){
    console.log('sign to the dashboard')
    this.headerData["X-Profile-Key"]=this.selectCompany
    this.loginService.signInData(this.userData,this.headerData)
        .subscribe(response=>{
          console.log(response)
          this.toasterService.pop('success',response.message)
          localStorage.setItem(this.mansysCloudTokenType,response.data.token_type);
          localStorage.setItem(this.mansysCloudAccessToken,response.data.access_token);
          localStorage.setItem(this.mansysCloudProfileName,response.data.profileName);
          localStorage.setItem(this.mansysCloudFullName,response.data.fullName);
          this.router.navigate(['/template']);
        },err=>{
          console.log(err)
          console.log('error','Error')
        },
        ()=>console.log('Get json Complete')
        )
  }

}
