import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('builder') builder:ElementRef;

  constructor() { }
  htmlText:string;

  ngOnInit() {
    this.htmlText = `<div class="row">
       <div class="s12 teal lighten-2">
        <p class="flow-text">div inside parent - html builder</p>
      </div>
    </div>`;

    //  this.builder.nativeElement.append(this.htmlText)
  }
  

}
