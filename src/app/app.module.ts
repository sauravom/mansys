import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule }  from './app.routing.modules';
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import {ToasterModule, ToasterService} from 'angular2-toaster';
import { LocalStorageModule } from 'angular-2-local-storage';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TemplateComponent } from './template/template.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { LoginService } from './services/login.service';
import { SellerService } from './services/seller.service';
import { GetJSONService } from './services/get-json.service';
import { SidebarDirective } from './directives/sidebar.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TemplateComponent,
    HomeComponent,
    SidebarDirective,
    SidebarComponent,
    HeaderComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ToasterModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    LocalStorageModule.withConfig({
            prefix: 'my-app',
            storageType: 'localStorage'
    })
  ],
  entryComponents:[
    AppComponent,
    LoginComponent,
    TemplateComponent,
    HomeComponent,
    SidebarComponent
  ],
  providers: [LoginService,
              SellerService,
              GetJSONService],
  bootstrap: [AppComponent]
})
export class AppModule { }
