import { MansysPage } from './app.po';

describe('mansys App', () => {
  let page: MansysPage;

  beforeEach(() => {
    page = new MansysPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
